# frozen_string_literal: true

require 'yaml'
require_relative 'user'
require_relative 'terminal'
require_relative 'operations'
require_relative 'message'

module Banking
  class Account
    include Transfer
    include Withdraw
    include Deposit
    include BalanceOperations
    def initialize(user_id)
      @user_id = user_id
      @user_account = []
      @user_account_id = []
      account_initializing
    end

    def account_initializing
      @accounts = DATABASE['accounts']
      @accounts.each_pair do |key, value|
        next unless @user_id == value['user_id']
        @user_account << @accounts[key]
        @user_account_id[0] = key if value['currency'].to_s == 'uah'
        @user_account_id[1] = key if value['currency'].to_s == 'usd'
      end
    end
  end
end
