# frozen_string_literal: true

require_relative 'terminal'
require_relative 'account'
require_relative 'operations'
require_relative 'message'

DATABASE = YAML.load_file(File.open(File.dirname(File.expand_path(__FILE__)) + '/database.yml'))

module Banking
  class User
    include Transfer
    include Withdraw
    include Deposit
    include Banking
    include BalanceOperations
    def initialize
      print "Enter Your Personal ID: \n>"
      @user_id = gets.to_i
      abort("\nTHERE ARE NO USER WITH THIS ID!") if DATABASE['users'][@user_id].nil?
      @user_name = DATABASE['users'][@user_id]['name']
      @user_pass = DATABASE['users'][@user_id]['password']
      call
    end

    def action_loop
      loop do
        print CHOOSE_OPTIONS
        action_case
      end
    end

    def call
      @transfer = 0
      print "Enter Your password: \n>"
      input = STDIN.gets.chomp
      abort("ACCOUNT AND PASSWORD DON'T MATCH!") unless input == @user_pass
      @account = Account.new(@user_id)
      @account.user_account_currencies
      action_loop
    end
  end
end
