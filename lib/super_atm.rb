# frozen_string_literal: true

require 'yaml'
require_relative 'user'
require_relative 'terminal'
require_relative 'account'
require_relative 'operations'
require_relative 'message'

DATABASE = YAML.load_file(File.open(File.dirname(File.expand_path(__FILE__)) + '/database.yml'))
H = %w[amount source_account target_account].freeze
USERS = DATABASE['users']
# H variable that makes transfer code shorter

def action_case
  case @action = gets.to_i
  when 1 then @account.display_balance
  when 2 then @account.deposit
  when 3 then @account.withdraw
  when 4 then @account.transfer_validation
  when 5
    @account.save
    abort("#{@user_name}, Thank You For Using Our ATM. Good-Bye!")
  else error(0)
  end
end

Banking::User.new
