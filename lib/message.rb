# frozen_string_literal: true

require 'yaml'
require_relative 'user'
require_relative 'terminal'
require_relative 'account'
require_relative 'operations'

ERROR = ["\nUNKNOWN COMMAND!", "\nINSUFFICIENT FUNDS!\n", "\nNEGATIVE AMOUNT!\n"].freeze
CHOOSE_OPTIONS = "\nPlease Choose the Action From the Following Options:
1. Display Balance
2. Deposit
3. Withdraw
4. Transfer
5. Log Out\n\n> "
MESSAGE = ["Select currency:\n1.UAH\n2.USD\n>", "Enter Amount You Wish to Deposit:\n>"].freeze
MESSAGE2 = ["Please enter receiver name:\n> "].freeze
def message(id)
  print MESSAGE[id] if id < 2
  print MESSAGE2[0] if id > 1
end

def error(id)
  puts ERROR[id]
end
