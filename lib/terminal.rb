# frozen_string_literal: true

require 'yaml'
require_relative 'user'
require_relative 'account'
require_relative 'operations'
require_relative 'message'

module Banking
  class Terminal
    def banknotes
      banknotes = DATABASE['banknotes']
      uah = banknotes['uah']
      usd = banknotes['usd']
      total_uah = 0
      uah.each_pair { |key, value| total_uah += key * value }
      total_usd = 0
      usd.each_pair { |key, value| total_usd += key * value }
      call
    end
  end
end
