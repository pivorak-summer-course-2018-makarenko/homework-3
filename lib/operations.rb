# frozen_string_literal: true

require 'yaml'
require_relative 'user'
require_relative 'terminal'
require_relative 'account'
require_relative 'message'

module Deposit
  def deposit
    @deposit = 1
    message(0)
    @input = gets.to_i
    message(1)
    @all_deposits = DATABASE['deposits']
    @last_deposit = 0
    @all_deposits.each_key do |key|
      @last_deposit = key
    end
    case_input
  end

  def case_input
    if @input == 1
      deposit_process(1)
    elsif @input == 2
      deposit_process(2)
    else
      error(0)
    end
  end

  def deposit_process(id)
    @amount = gets.to_i
    abort(ERROR[3]) if @amount.negative?
    uah_deposit if id == 1
    usd_deposit if id == 2
    @all_deposits.merge!(@dep)
  end

  def uah_deposit
    @user_account[0]['balance'] = @usr_acc_uah_b + @amount
    @usr_acc_uah_b += @amount
    puts "Great, Now your balance is #{@usr_acc_uah_b} UAH"
    @accounts[@usr_h_id] = @usr_acc_uah
    @dep = { @last_deposit + 1 => { 'amount' => @amount, 'account_id' => @usr_h_id } }
  end

  def usd_deposit
    @user_account[1]['balance'] = @usr_acc_usd['balance'] + @amount
    @usr_acc_usd_b += @amount
    puts "Great, Now your balance is #{@usr_acc_usd['balance']} USD"
    @accounts[@usr_d_id] = @usr_acc_usd
    @dep = { @last_deposit + 1 => { 'amount' => @amount, 'account_id' => @usr_d_id } }
  end
end

module Transfer
  def target_account_currencies
    default_target_account_configuration
    @target_account.each do |account|
      case account['currency'].to_s
      when 'uah' then target_account_uah_configuration(account)
      when 'usd' then target_account_usd_configuration(account)
      else next
      end
      @id += 1
    end
  end

  def default_target_account_configuration
    @id = 0
    @hash = DATABASE
    @target_account = []
    @target_account_id = []
    @accounts.each_pair do |key, value|
      if @target_user_id == value['user_id']
        @target_account << @accounts[key]
        @target_account_id << key
      end
    end
  end

  def target_account_uah_configuration(account)
    @t_acc_uah = account
    @tar_acc_uah_id = @target_account_id[@id]
  end

  def target_account_usd_configuration(account)
    @t_acc_usd = account
    @tar_acco_usd_id = @target_account_id[@id]
  end

  def transfer_validation
    message(2)
    input_name = gets.chomp
    USERS.each_pair do |key, value|
      next unless input_name == value['name'].to_s
      abort("\nYou can't transfer to yourself!") if input_name == USERS[@user_id]['name']
      @target_user_id = key
      transfer
    end
    puts "\nTHERE ARE NO RECEIVER WITH THIS NAME! \n" if @target_user_id.nil?
  end

  def transfer
    @transfer = 1
    target_account_currencies
    transfer_configuration
    @currency = gets.to_i
    error(0) unless @currency == 1 || @currency == 2
    print "Please enter amount:\n>"
    transfer_process
    @lst_tr += 1
    uah_transfer if @currency == 1
    usd_transfer if @currency == 2
  end

  def transfer_configuration
    @all_transfers = DATABASE['transfers']
    message(0)
    @lst_tr = 0
    @all_transfers.each_key do |key|
      @lst_tr = key
    end
  end

  def transfer_process
    @all_ac = DATABASE['accounts']
    @uah_id = [@usr_h_id, @tar_acc_uah_id]
    @usd_id = [@usr_d_id, @tar_acc_usd_id]
    @usd = [@usr_acc_usd_b, @t_acc_usd['balance'], @usr_acc_usd_b, @usr_acc_usd, @all_ac[@usr_d_id]]
    @uah = [@usr_acc_uah_b, @t_acc_uah['balance'], @usr_acc_uah_b, @usr_acc_uah, @all_ac[@usr_h_id]]
    @uah = @usd if @currency == 2
    @uah_id = @usd_id if @currency == 2
    @transfer_amount = gets.to_i
    transfer_balance_verifying
    @all_transfers.merge!(@new_t)
  end

  def transfer_balance_verifying
    error(1) if (@uah[0] - @transfer_amount).negative?
    transfer_balance_editing
    puts "\nGreat, Now your balance is #{@uah[0]} UAH\n"
    @new_t = { @lst_tr => { H[0] => @transfer_amount, H[1] => @uah_id[2], H[2] => @uah_id[1] } }
    abort("\nYOU CAN'T TRANSFER TO YOURSELF!\n") if @transfer_amount.negative?
  end

  def transfer_balance_editing
    @uah[1] += @transfer_amount
    @uah[0] -= @transfer_amount
    @uah[3]['balance'] = @uah[0]
    @uah[4] = @usr_acc_uah
  end

  def uah_transfer
    @usr_acc_uah_b = @uah[0]
    @t_acc_uah['balance'] = @uah[1]
    @usr_acc_uah = @uah[3]
    @all_ac[@usr_h_id] = @uah[4]
  end

  def usd_transfer
    @usr_acc_usd_b = @uah[0]
    @t_acc_usd['balance'] = @uah[1]
    @usr_acc_usd = @uah[3]
    @all_ac[@usr_d_id] = @uah[4]
  end
end

module Withdraw
  def withdraw_configuration
    @withdraw = 1
    @last_withdraw = 0
    @all_withdraws = DATABASE['withdraws']
    @all_withdraws.each_key do |key|
      @last_withdraw = key
    end
  end

  def withdraw
    withdraw_configuration
    message(0)
    input = gets.to_i
    message(1)
    case input
    when 1 then withdraw_process(1)
    when 2 then withdraw_process(2)
    else error(0)
    end
  end

  def withdraw_process(id)
    @amount = gets.to_i
    abort(ERROR[3]) if @amount.negative?
    @last_withdraw += 1
    if id == 1
      uah_withdraw
    else
      usd_withdraw
    end
    @all_withdraws.merge!(@new_withdraw)
  end

  def uah_withdraw
    abort(ERROR[1]) if (@usr_acc_uah_b - @amount).negative?
    @user_account[0]['balance'] = @usr_acc_uah_b - @amount
    @usr_acc_uah_b -= @amount
    @accounts[@usr_h_id] = @usr_acc_uah
    puts "Great, Now your balance is #{@usr_acc_uah_b} UAH"
    @new_withdraw = { @last_withdraw => { 'amount' => @amount, 'account_id' => @usr_h_id } }
  end

  def usd_withdraw
    abort(ERROR[1]) if (@usr_acc_usd_b - @amount).negative?
    @user_account[1]['balance'] = @usr_acc_usd_b - @amount
    @usr_acc_usd_b -= @amount
    @accounts[@usr_d_id] = @usr_acc_usd
    puts "Great, Now your balance is #{@usr_acc_usd_b} USD"
    @new_withdraw = { @last_withdraw => { 'amount' => @amount, 'account_id' => @usr_d_id } }
  end

  def user_account_currencies
    id = 0
    @user_account.each do |account|
      case account['currency'].to_s
      when 'uah' then account_uah_configuration(account)
      when 'usd' then account_usd_configuration(account)
      else next
      end
      id += 1
    end
  end

  def account_uah_configuration(account)
    @usr_acc_uah = account
    @usr_h_id = @user_account_id[0]
    @usr_acc_uah_b = @usr_acc_uah['balance']
  end

  def account_usd_configuration(account)
    @usr_acc_usd = account
    @usr_d_id = @user_account_id[1]
    @usr_acc_usd_b = @usr_acc_usd['balance']
  end
end
module BalanceOperations
  def display_balance
    print "\nYour  Current Balance is:"
    @user_account.each do |ac|
      print " #{ac['balance']} #{ac['currency'].upcase} "
    end
    print "\n"
  end

  def save
    target_account_currencies
    deposit_save
    if @transfer == 1
      @hash['accounts'][@tar_acc_uah_id]['balance'] = @t_acc_uah['balance']
      @hash['accounts'][@tar_acco_usd_id]['balance'] = @t_acc_usd['balance']
      @hash['transfers'] = @all_transfers
    end
    File.open('database.yml', 'w') do |file|
      file.puts @hash.to_yaml
    end
  end

  def deposit_save
    @hash['accounts'][@usr_h_id]['balance'] = @usr_acc_uah_b
    @hash['accounts'][@usr_d_id]['balance'] = @usr_acc_usd_b
    @hash['withdraws'] = @all_withdraws if @withdraw == 1
    @hash['deposits'] = @all_deposits if @deposits == 1
  end
end
