# frozen_string_literal: true

require 'user'

describe Banking::User do
  describe 'initialize' do
    it 'User id test' do
      user = Banking::User.new
      User.user_id.stub(gets: 3321)
      User.user_pass.stub(gets: '3')
      expect { user }.to output("ACCOUNT AND PASSWORD DON'T MATCH!")
    end
  end
end
