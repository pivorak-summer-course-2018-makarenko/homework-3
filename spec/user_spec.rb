# frozen_string_literal: true

require 'user'

describe Banking::User do
  describe 'initialize' do
    it 'User id test' do
      user = Banking::User.new
      allow(user).to receive(:gets)
      # user.stub!(:gets) { }
      expect { user.user_validation }.to output("ACCOUNT AND PASSWORD DON'T MATCH!").to_stdout
    end
  end
end
